package io.chelo.reyes.searchml.core.mappers

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.chelo.reyes.searchml.core.SEARCH_RESPONSE_PAYLOAD
import io.chelo.reyes.searchml.core.TestUtils
import io.chelo.reyes.searchml.core.models.item.InstallmentsModelInterface
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface
import io.chelo.reyes.searchml.core.models.item.PicturesItemModelInterface
import io.chelo.reyes.searchml.core.models.item.ShippingModelInterface
import io.chelo.reyes.searchml.core.models.response.PagingModelInterface
import io.chelo.reyes.searchml.core.models.response.QueryResponseModelInterface
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class QueryResponseAdapterTest {
    lateinit var gson: Gson

    @Before
    fun init() {
        gson = GsonBuilder().apply {
            registerTypeAdapter(
                QueryResponseModelInterface::class.java,
                QueryResponseAdapter.QueryResponseModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PagingModelInterface::class.java,
                QueryResponseAdapter.PagingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ShippingModelInterface::class.java,
                ItemModelAdapter.ShippingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PicturesItemModelInterface::class.java,
                ItemModelAdapter.PicturesItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                InstallmentsModelInterface::class.java,
                ItemModelAdapter.InstallmentsModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
        }.create()
    }

    @Test
    fun queryResponseAdapter_Deserializer_NotNull() {
        //Given
        val responseJson = TestUtils.loadAsJson(SEARCH_RESPONSE_PAYLOAD)
        //When
        val responseObject = gson.fromJson(responseJson, QueryResponseModelInterface::class.java)
        //Then
        assertNotNull(responseObject)
        assertNotNull(responseObject.paging)
        assertNotNull(responseObject.results)
        assert(responseObject.results.isNotEmpty())
    }
}