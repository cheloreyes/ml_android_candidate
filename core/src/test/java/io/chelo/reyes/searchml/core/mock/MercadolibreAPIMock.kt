package io.chelo.reyes.searchml.core.mock

import com.google.gson.JsonElement
import io.chelo.reyes.searchml.core.ITEM_DETAIL_PAYLOAD
import io.chelo.reyes.searchml.core.SEARCH_RESPONSE_PAYLOAD
import io.chelo.reyes.searchml.core.TestUtils
import io.chelo.reyes.searchml.core.services.MercadoLibreAPI
import io.reactivex.rxjava3.core.Single

class MercadolibreAPIMock : MercadoLibreAPI {
    override fun searchInSite(siteId: String, query: String): Single<JsonElement> =
        Single.just(TestUtils.loadAsJson(SEARCH_RESPONSE_PAYLOAD))

    override fun getItemDetail(id: String): Single<JsonElement> =
        Single.just(TestUtils.loadAsJson(ITEM_DETAIL_PAYLOAD))
}