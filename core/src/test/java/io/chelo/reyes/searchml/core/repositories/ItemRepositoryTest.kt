package io.chelo.reyes.searchml.core.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.chelo.reyes.searchml.core.di.CoreModule
import io.chelo.reyes.searchml.core.di.DaggerTestCoreComponent
import io.chelo.reyes.searchml.core.di.TestCoreModule
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

class ItemRepositoryTest {
    @Inject
    lateinit var itemRepository: ItemRepository

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val component = DaggerTestCoreComponent.builder()
            .testCoreModule(TestCoreModule(CoreModule()))
            .build()
        component.inject(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun itemRepository_getItemDetails_Success() {
        //Given
        val itemId = "MCO488488170"
        val request = itemRepository.getItemDetails(itemId)
        val response = TestObserver<ItemModelInterface>()

        //When
        request.subscribe(response)

        //Then
        response.assertNoErrors()
        response.await()
        response.assertComplete()
        response.dispose()
    }

    @Test
    fun itemRepository_getItemDetails_ValidData() {
        //Given
        val itemId = "MCO488488170"
        val request = itemRepository.getItemDetails(itemId)
        val response = TestObserver<ItemModelInterface>()

        //When
        request.subscribe(response)

        //Then
        response.assertNoErrors()
        response.await()
        response.assertValue {
            it.id == itemId
        }
        response.assertComplete()
        response.dispose()
    }
}