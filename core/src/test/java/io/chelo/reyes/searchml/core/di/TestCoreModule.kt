package io.chelo.reyes.searchml.core.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.chelo.reyes.searchml.core.mock.MercadolibreAPIMock
import io.chelo.reyes.searchml.core.repositories.ItemRepository
import io.chelo.reyes.searchml.core.repositories.SearchRepository
import io.chelo.reyes.searchml.core.services.MercadoLibreAPI

@Module
class TestCoreModule(private val coreModule: CoreModule) {
    @Provides
    fun provideGson(): Gson = coreModule.provideGson()

    @Provides
    fun provideMercadolibreAPIMock(): MercadoLibreAPI = MercadolibreAPIMock()

    @Provides
    fun provideSearchRepository(mercadoLibreAPI: MercadoLibreAPI, gson: Gson): SearchRepository =
        SearchRepository(mercadoLibreAPI, gson)

    @Provides
    fun provideItemRepository(mercadoLibreAPI: MercadoLibreAPI, gson: Gson): ItemRepository =
        ItemRepository(mercadoLibreAPI, gson)
}