package io.chelo.reyes.searchml.core.mappers

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.chelo.reyes.searchml.core.ITEM_DETAIL_PAYLOAD
import io.chelo.reyes.searchml.core.TestUtils
import io.chelo.reyes.searchml.core.models.item.*
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class ItemModelAdapterTest {
    lateinit var gson: Gson

    @Before
    fun init() {
        gson = GsonBuilder().apply {
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ShippingModelInterface::class.java,
                ItemModelAdapter.ShippingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PicturesItemModelInterface::class.java,
                ItemModelAdapter.PicturesItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                InstallmentsModelInterface::class.java,
                ItemModelAdapter.InstallmentsModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ConditionType::class.java,
                ItemDetailResponseAdapter.ConditionTypeAdapter()
            )
        }.create()
    }

    @Test
    fun itemModelAdapter_Deserialize_NotNull() {
        //Given
        val json = TestUtils.loadAsJson(ITEM_DETAIL_PAYLOAD)
        val itemJson = json.asJsonArray.get(0).asJsonObject.get("body")

        //When
        val itemModelInterface = gson.fromJson(itemJson, ItemModelInterface::class.java)
        //Then
        assertNotNull(itemModelInterface)
        assertNotNull(itemModelInterface.pictures)
        assertNotNull(itemModelInterface.shipping)
    }

    @Test
    fun itemModelAdapter_Deserialize_RightValues() {
        //Given
        val json = TestUtils.loadAsJson(ITEM_DETAIL_PAYLOAD)
        val itemJson = json.asJsonArray.get(0).asJsonObject.get("body")

        //When
        val itemModelInterface = gson.fromJson(itemJson, ItemModelInterface::class.java)
        //Then
        assertNotNull(itemModelInterface)
        assert(itemModelInterface.id == "MCO488488170")
        assert(itemModelInterface.price == 179900.0)
        assert(itemModelInterface.currencyId == "COP")
    }
}