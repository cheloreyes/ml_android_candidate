package io.chelo.reyes.searchml.core.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.chelo.reyes.searchml.core.di.CoreModule
import io.chelo.reyes.searchml.core.di.DaggerTestCoreComponent
import io.chelo.reyes.searchml.core.di.TestCoreModule
import io.chelo.reyes.searchml.core.models.response.QueryResponseModelInterface
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

class SearchRepositoryTest {
    @Inject
    lateinit var searchRepository: SearchRepository

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val component = DaggerTestCoreComponent.builder()
            .testCoreModule(TestCoreModule(CoreModule()))
            .build()
        component.inject(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun searchRepository_search_success() {
        //Given
        val request = searchRepository.search("")
        val response = TestObserver<QueryResponseModelInterface>()

        //When
        request.subscribe(response)

        //Then
        response.assertNoErrors()
        response.await()
        response.assertComplete()
        response.dispose()
    }

    @Test
    fun searchRepository_search_validValues() {
        //Given
        val request = searchRepository.search("")
        val response = TestObserver<QueryResponseModelInterface>()

        //When
        request.subscribe(response)

        //Then
        response.assertNoErrors()
        response.assertValue {
            it.paging.total > 0 &&
                    it.paging.limit > 0 &&
                    it.results.isNotEmpty() &&
                    it.results.first().id.isNotEmpty()
        }
        response.assertComplete()
        response.dispose()
    }
}