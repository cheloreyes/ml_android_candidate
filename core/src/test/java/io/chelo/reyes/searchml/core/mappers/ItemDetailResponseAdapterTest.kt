package io.chelo.reyes.searchml.core.mappers

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.chelo.reyes.searchml.core.ITEM_DETAIL_PAYLOAD
import io.chelo.reyes.searchml.core.TestUtils
import io.chelo.reyes.searchml.core.models.item.*
import io.chelo.reyes.searchml.core.models.response.ItemDetailResponseModelInterface
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class ItemDetailResponseAdapterTest {
    lateinit var gson: Gson

    @Before
    fun init() {
        gson = GsonBuilder().apply {
            registerTypeAdapter(
                ItemDetailResponseModelInterface::class.java,
                ItemDetailResponseAdapter.ItemDetailResponseModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ShippingModelInterface::class.java,
                ItemModelAdapter.ShippingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PicturesItemModelInterface::class.java,
                ItemModelAdapter.PicturesItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                InstallmentsModelInterface::class.java,
                ItemModelAdapter.InstallmentsModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ConditionType::class.java,
                ItemDetailResponseAdapter.ConditionTypeAdapter()
            )
        }.create()
    }

    @Test
    fun itemDetailResponseAdapter_Deserialize_NotNull() {
        //Given
        val jsonItemDetail = TestUtils.loadAsJson(ITEM_DETAIL_PAYLOAD)
        val typeToken = object : TypeToken<List<ItemDetailResponseModelInterface>>() {}.type
        //When
        val deserializeObject =
            gson.fromJson<List<ItemDetailResponseModelInterface>>(jsonItemDetail, typeToken)
        //Then

        assertNotNull(deserializeObject)
        assert(deserializeObject.isNotEmpty())
        assert(deserializeObject.first().code == 200)
        assertNotNull(deserializeObject.first().body)
    }
}