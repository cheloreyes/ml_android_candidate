package io.chelo.reyes.searchml.core.di

import dagger.Component
import io.chelo.reyes.searchml.core.repositories.ItemRepositoryTest
import io.chelo.reyes.searchml.core.repositories.SearchRepositoryTest
import javax.inject.Singleton

@Singleton
@Component(
    modules = [TestCoreModule::class]
)
interface TestCoreComponent {
    fun inject(test: SearchRepositoryTest)
    fun inject(test: ItemRepositoryTest)
}