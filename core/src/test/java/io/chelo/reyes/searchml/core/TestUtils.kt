package io.chelo.reyes.searchml.core

import com.google.gson.Gson
import com.google.gson.JsonElement

internal const val ITEM_DETAIL_PAYLOAD: String = "test_item_detail_payload.json"
internal const val SEARCH_RESPONSE_PAYLOAD: String = "test_search_result_payload.json"

object TestUtils {
    fun loadRawJson(name: String): String =
        try {
            object {}.javaClass.classLoader?.getResource("raw/$name")
                ?.readText(Charsets.UTF_8) ?: ""
        } catch (all: Exception) {
            throw RuntimeException("Failed to load json=$name", all)
        }

    fun loadAsJson(resource: String): JsonElement {
        val raw = loadRawJson(resource)
        return Gson().fromJson(raw, JsonElement::class.java)
    }
}