package io.chelo.reyes.searchml.core.repositories

import com.google.gson.Gson
import io.chelo.reyes.searchml.core.BuildConfig
import io.chelo.reyes.searchml.core.models.response.QueryResponseModelInterface
import io.chelo.reyes.searchml.core.services.MercadoLibreAPI
import io.reactivex.rxjava3.core.Single

/**
 * This repository expose a generic access to data actions, without many specifications for the view.
 * @param mercadoLibreAPI The backend service to access to remote data.
 * @param gson Bailed gson with own serialized configuration.
 */
class SearchRepository(private val mercadoLibreAPI: MercadoLibreAPI, private val gson: Gson) {

    fun search(query: String): Single<QueryResponseModelInterface> {
        return mercadoLibreAPI.searchInSite(BuildConfig.SITE_ID, query)
            .map { gson.fromJson(it, QueryResponseModelInterface::class.java) }
    }
}