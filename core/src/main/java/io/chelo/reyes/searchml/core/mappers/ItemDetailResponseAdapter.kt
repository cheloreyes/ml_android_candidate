package io.chelo.reyes.searchml.core.mappers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import io.chelo.reyes.searchml.core.deserialize
import io.chelo.reyes.searchml.core.models.item.ConditionType
import io.chelo.reyes.searchml.core.models.response.ItemDetailResponseModel
import io.chelo.reyes.searchml.core.models.response.ItemDetailResponseModelInterface
import java.lang.reflect.Type

object ItemDetailResponseAdapter {
    class ItemDetailResponseModelInterfaceAdapter :
        JsonDeserializer<ItemDetailResponseModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): ItemDetailResponseModelInterface = deserialize<ItemDetailResponseModel>(json, context)
    }

    class ConditionTypeAdapter :
        JsonDeserializer<ConditionType> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): ConditionType = ConditionType.parseCondition(json?.asString ?: "")
    }
}