package io.chelo.reyes.searchml.core.mappers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import io.chelo.reyes.searchml.core.deserialize
import io.chelo.reyes.searchml.core.models.item.*
import java.lang.reflect.Type

object ItemModelAdapter {
    class ShippingModelInterfaceAdapter : JsonDeserializer<ShippingModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): ShippingModelInterface = deserialize<ShippingModel>(json, context)
    }

    class PicturesItemModelInterfaceAdapter : JsonDeserializer<PicturesItemModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): PicturesItemModelInterface = deserialize<PicturesItemModel>(json, context)
    }

    class InstallmentsModelInterfaceAdapter : JsonDeserializer<InstallmentsModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): InstallmentsModelInterface = deserialize<InstallmentsModel>(json, context)
    }
}