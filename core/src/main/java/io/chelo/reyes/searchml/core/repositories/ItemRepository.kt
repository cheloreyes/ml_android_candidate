package io.chelo.reyes.searchml.core.repositories

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface
import io.chelo.reyes.searchml.core.models.response.ItemDetailResponseModelInterface
import io.chelo.reyes.searchml.core.services.MercadoLibreAPI
import io.reactivex.rxjava3.core.Single

class ItemRepository(private val mercadoLibreAPI: MercadoLibreAPI, private val gson: Gson) {
    fun getItemDetails(id: String): Single<ItemModelInterface> {
        return mercadoLibreAPI.getItemDetail(id)
            .map {
                val typeToken = object : TypeToken<List<ItemDetailResponseModelInterface>>() {}.type
                gson.fromJson<List<ItemDetailResponseModelInterface>>(it, typeToken).first()
                    .body
            }
    }
}