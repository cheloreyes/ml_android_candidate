package io.chelo.reyes.searchml.core.models.item

import com.google.gson.annotations.SerializedName

interface ShippingModelInterface {
    val freeShipping: Boolean
}

data class ShippingModel(
    @SerializedName("free_shipping")
    override val freeShipping: Boolean = false
) : ShippingModelInterface