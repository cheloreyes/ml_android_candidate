package io.chelo.reyes.searchml.core.models.response

import com.google.gson.annotations.SerializedName

interface PagingModelInterface {
    val total: Int
    val offset: Int
    val limit: Int
    val primaryResults: Int
}

data class PagingModel(
    @SerializedName("total")
    override val total: Int = 0,
    @SerializedName("offset")
    override val offset: Int = 0,
    @SerializedName("limit")
    override val limit: Int = 0,
    @SerializedName("primary_results")
    override val primaryResults: Int = 0
) : PagingModelInterface