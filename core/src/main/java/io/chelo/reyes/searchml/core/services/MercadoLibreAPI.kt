package io.chelo.reyes.searchml.core.services

import com.google.gson.JsonElement
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


internal const val KEY_SITE_ID = "siteId"
internal const val PATH_SITE_ID = "{siteId}"
internal const val FIELD_QUERY = "q"
private const val SEARCH_URL: String = "sites/$PATH_SITE_ID/search"
private const val ITEM_DETAIL_URL: String = "items"
internal const val FIELD_IDS = "ids"

/**
 * Service to grant access to available resources for this exercise provided in:
 * http://developers.mercadolibre.com/items-and-searches/
 */
interface MercadoLibreAPI {

    /**
     * Request a search of items related to given [query] of a related [siteId].
     * @return JsonElement with the response.
     */
    @GET(SEARCH_URL)
    fun searchInSite(
        @Path(KEY_SITE_ID) siteId: String,
        @Query(FIELD_QUERY) query: String
    ): Single<JsonElement>

    /**
     * Request the item detail by [id]
     * @return JsonElement with the service response.
     */
    @GET(ITEM_DETAIL_URL)
    fun getItemDetail(
        @Query(FIELD_IDS) id: String
    ): Single<JsonElement>
}