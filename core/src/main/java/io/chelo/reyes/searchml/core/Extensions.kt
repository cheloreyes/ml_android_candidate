package io.chelo.reyes.searchml.core

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement

/**
 * This function grant a direct access to [deserialize] function of [JsonDeserializationContext]
 * or @throws [NullPointerException].
 */
inline fun <reified T> deserialize(json: JsonElement?, context: JsonDeserializationContext?): T {
    return context?.deserialize(json, T::class.java) ?: throw NullPointerException("null context")
}