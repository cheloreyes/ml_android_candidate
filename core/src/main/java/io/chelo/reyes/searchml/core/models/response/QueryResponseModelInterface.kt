package io.chelo.reyes.searchml.core.models.response

import com.google.gson.annotations.SerializedName
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface

interface QueryResponseModelInterface {
    val paging: PagingModelInterface
    val results: List<ItemModelInterface>
}

/**
 * This is the model abstraction for the scope of this exercise.
 */
data class QueryResponseModel(
    @SerializedName("paging")
    override val paging: PagingModelInterface,
    @SerializedName("results")
    override val results: List<ItemModelInterface>
) : QueryResponseModelInterface