package io.chelo.reyes.searchml.core.mappers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import io.chelo.reyes.searchml.core.deserialize
import io.chelo.reyes.searchml.core.models.item.ItemModel
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface
import io.chelo.reyes.searchml.core.models.response.PagingModel
import io.chelo.reyes.searchml.core.models.response.PagingModelInterface
import io.chelo.reyes.searchml.core.models.response.QueryResponseModel
import io.chelo.reyes.searchml.core.models.response.QueryResponseModelInterface
import java.lang.reflect.Type

object QueryResponseAdapter {
    class QueryResponseModelInterfaceAdapter : JsonDeserializer<QueryResponseModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): QueryResponseModelInterface = deserialize<QueryResponseModel>(json, context)

    }

    class PagingModelInterfaceAdapter : JsonDeserializer<PagingModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): PagingModelInterface = deserialize<PagingModel>(json, context)
    }

    class ItemModelInterfaceAdapter : JsonDeserializer<ItemModelInterface> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): ItemModelInterface = deserialize<ItemModel>(json, context)
    }
}