package io.chelo.reyes.searchml.core.models.item

import com.google.gson.annotations.SerializedName

interface InstallmentsModelInterface {
    val amount: Double
    val quantity: Int
    val currencyId: String
}

data class InstallmentsModel(
    @SerializedName("amount")
    override val amount: Double = 0.0,
    @SerializedName("quantity")
    override val quantity: Int = 0,
    @SerializedName("currency_id")
    override val currencyId: String = ""
) : InstallmentsModelInterface