package io.chelo.reyes.searchml.core.models.response

import com.google.gson.annotations.SerializedName
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface

interface ItemDetailResponseModelInterface {
    val code: Int
    val body: ItemModelInterface
}

data class ItemDetailResponseModel(
    @SerializedName("code")
    override val code: Int = 0,
    @SerializedName("body")
    override val body: ItemModelInterface
) : ItemDetailResponseModelInterface