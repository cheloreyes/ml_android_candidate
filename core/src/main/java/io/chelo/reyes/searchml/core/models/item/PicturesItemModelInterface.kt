package io.chelo.reyes.searchml.core.models.item

import com.google.gson.annotations.SerializedName

interface PicturesItemModelInterface {
    val size: String
    val secureUrl: String
    val id: String
    val url: String
    val maxSize: String
    val quality: String
}

data class PicturesItemModel(
    @SerializedName("size")
    override val size: String = "",
    @SerializedName("secure_url")
    override val secureUrl: String = "",
    @SerializedName("id")
    override val id: String = "",
    @SerializedName("url")
    override val url: String = "",
    @SerializedName("max_size")
    override val maxSize: String = "",
    @SerializedName("quality")
    override val quality: String = ""
) : PicturesItemModelInterface
