package io.chelo.reyes.searchml.core.models.item

import com.google.gson.annotations.SerializedName

interface ItemModelInterface {
    val id: String
    val title: String
    val originalPrice: Double
    val availableQuantity: Int
    val shipping: ShippingModelInterface
    val price: Double
    val acceptsMercadopago: Boolean
    val soldQuantity: Int
    val pictures: List<PicturesItemModelInterface>
    val initialQuantity: Int
    val currencyId: String
    val condition: ConditionType
    val installments: InstallmentsModelInterface?
    val thumbnail: String
}

data class ItemModel(
    @SerializedName("original_price")
    override val originalPrice: Double = 0.0,
    @SerializedName("available_quantity")
    override val availableQuantity: Int = 0,
    @SerializedName("shipping")
    override val shipping: ShippingModelInterface,
    @SerializedName("price")
    override val price: Double = 0.0,
    @SerializedName("id")
    override val id: String = "",
    @SerializedName("accepts_mercadopago")
    override val acceptsMercadopago: Boolean = false,
    @SerializedName("title")
    override val title: String = "",
    @SerializedName("sold_quantity")
    override val soldQuantity: Int = 0,
    @SerializedName("pictures")
    override val pictures: List<PicturesItemModelInterface>,
    @SerializedName("initial_quantity")
    override val initialQuantity: Int = 0,
    @SerializedName("currency_id")
    override val currencyId: String = "",
    @SerializedName("installments")
    override val installments: InstallmentsModelInterface?,
    @SerializedName("thumbnail")
    override val thumbnail: String = "",
    @SerializedName("condition")
    override val condition: ConditionType
) : ItemModelInterface