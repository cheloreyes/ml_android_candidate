package io.chelo.reyes.searchml.core.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.chelo.reyes.searchml.core.BuildConfig
import io.chelo.reyes.searchml.core.mappers.ItemDetailResponseAdapter
import io.chelo.reyes.searchml.core.mappers.ItemModelAdapter
import io.chelo.reyes.searchml.core.mappers.QueryResponseAdapter
import io.chelo.reyes.searchml.core.models.item.*
import io.chelo.reyes.searchml.core.models.response.ItemDetailResponseModelInterface
import io.chelo.reyes.searchml.core.models.response.PagingModelInterface
import io.chelo.reyes.searchml.core.models.response.QueryResponseModelInterface
import io.chelo.reyes.searchml.core.repositories.ItemRepository
import io.chelo.reyes.searchml.core.repositories.SearchRepository
import io.chelo.reyes.searchml.core.services.MercadoLibreAPI
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * This Dagger module provides the declarations to build the data access for the ViewModelFactories
 * throw generic ways with repositories.
 */
@Module
class CoreModule {
    @Provides
    fun provideGson(): Gson =
        GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).apply {
            registerTypeAdapter(
                QueryResponseModelInterface::class.java,
                QueryResponseAdapter.QueryResponseModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PagingModelInterface::class.java,
                QueryResponseAdapter.PagingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ShippingModelInterface::class.java,
                ItemModelAdapter.ShippingModelInterfaceAdapter()
            )
            registerTypeAdapter(
                PicturesItemModelInterface::class.java,
                ItemModelAdapter.PicturesItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                InstallmentsModelInterface::class.java,
                ItemModelAdapter.InstallmentsModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemModelInterface::class.java,
                QueryResponseAdapter.ItemModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ItemDetailResponseModelInterface::class.java,
                ItemDetailResponseAdapter.ItemDetailResponseModelInterfaceAdapter()
            )
            registerTypeAdapter(
                ConditionType::class.java,
                ItemDetailResponseAdapter.ConditionTypeAdapter()
            )
        }.create()

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .addNetworkInterceptor(StethoInterceptor())
        .build()

    @Provides
    fun provideMercadoLibreAPI(retrofit: Retrofit): MercadoLibreAPI =
        retrofit.create(MercadoLibreAPI::class.java)

    @Provides
    fun provideSearchRepository(mercadoLibreAPI: MercadoLibreAPI, gson: Gson): SearchRepository =
        SearchRepository(mercadoLibreAPI, gson)

    @Provides
    fun provideItemRepository(mercadoLibreAPI: MercadoLibreAPI, gson: Gson): ItemRepository =
        ItemRepository(mercadoLibreAPI, gson)
}