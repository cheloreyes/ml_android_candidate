package io.chelo.reyes.searchml.core.models.item

sealed class ConditionType {
    abstract val condition: String

    companion object {
        fun parseCondition(rawCondition: String): ConditionType {
            return when (rawCondition) {
                New.condition -> New
                Used.condition -> Used
                else -> Undefined(rawCondition)
            }
        }
    }

    object New : ConditionType() {
        override val condition: String = "new"
    }

    object Used : ConditionType() {
        override val condition: String = "used"

    }

    class Undefined(override val condition: String) : ConditionType()
}