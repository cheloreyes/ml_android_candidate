package io.chelo.reyes.searchml.item.view

import android.widget.TextView
import androidx.databinding.BindingAdapter
import io.chelo.reyes.searchml.R
import io.chelo.reyes.searchml.core.models.item.ConditionType

@BindingAdapter(value = ["item:installments", "item:installment_price"])
fun setInstallmentValues(textView: TextView, installments: Int?, installmentsPrice: String?) {
    val installmentsFormatted = textView.context.getString(
        R.string.item_installments_format,
        installments ?: 0,
        installmentsPrice ?: ""
    )
    textView.text = installmentsFormatted
}

@BindingAdapter("item:condition")
fun setItemCondition(textView: TextView, conditionType: ConditionType?) {
    conditionType?.let {
        val conditionRes = if (conditionType == ConditionType.Used) R.string.item_condition_used
        else R.string.item_condition_new
        textView.text = textView.context.getString(conditionRes)
    }
}
