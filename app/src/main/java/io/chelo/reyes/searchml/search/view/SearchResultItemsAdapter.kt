package io.chelo.reyes.searchml.search.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import io.chelo.reyes.searchml.databinding.ItemEmptyStateLayoutBinding
import io.chelo.reyes.searchml.databinding.ItemLayoutBinding
import io.chelo.reyes.searchml.databinding.ItemShimmerLayoutBinding
import io.chelo.reyes.searchml.search.viewmodel.LoadingItem
import io.chelo.reyes.searchml.search.viewmodel.ResultItemViewModel
import io.chelo.reyes.searchml.search.viewmodel.SearchResultItem

class SearchResultItemsAdapter :
    ListAdapter<SearchResultItem, SearchResultItemsViewHolder>(
        SearchResultItemDiffCallback()
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultItemsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            SearchResultItemsViewHolder.Item.viewType -> {
                val binding = DataBindingUtil.inflate<ItemLayoutBinding>(
                    inflater,
                    viewType,
                    parent,
                    false
                )
                SearchResultItemsViewHolder.Item(
                    binding
                )
            }
            SearchResultItemsViewHolder.LoadingState.viewType -> {
                val binding = DataBindingUtil.inflate<ItemShimmerLayoutBinding>(
                    inflater,
                    viewType,
                    parent,
                    false
                )
                SearchResultItemsViewHolder.LoadingState(binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<ItemEmptyStateLayoutBinding>(
                    inflater,
                    viewType,
                    parent,
                    false
                )
                SearchResultItemsViewHolder.EmptyState(
                    binding
                )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ResultItemViewModel -> SearchResultItemsViewHolder.Item.viewType
            is LoadingItem -> SearchResultItemsViewHolder.LoadingState.viewType
            else -> SearchResultItemsViewHolder.EmptyState.viewType
        }
    }

    override fun onBindViewHolder(holder: SearchResultItemsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class SearchResultItemDiffCallback : DiffUtil.ItemCallback<SearchResultItem>() {
        override fun areItemsTheSame(
            oldItem: SearchResultItem,
            newItem: SearchResultItem
        ): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(
            oldItem: SearchResultItem,
            newItem: SearchResultItem
        ): Boolean =
            oldItem == newItem
    }
}