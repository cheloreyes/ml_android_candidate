package io.chelo.reyes.searchml.search.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.chelo.reyes.searchml.core.repositories.SearchRepository
import io.chelo.reyes.searchml.di.AppComponentManager
import javax.inject.Inject

class SearchViewModelFactory : ViewModelProvider.Factory {
    @Inject
    lateinit var searchRepository: SearchRepository

    init {
        AppComponentManager.appComponent.inject(this)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return modelClass.getConstructor(SearchRepository::class.java)
                .newInstance(searchRepository)
        }
        throw IllegalStateException("ViewModel must extend ${modelClass.name}")
    }
}