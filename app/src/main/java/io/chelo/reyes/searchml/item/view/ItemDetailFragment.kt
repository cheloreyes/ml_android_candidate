package io.chelo.reyes.searchml.item.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import io.chelo.reyes.searchml.R
import io.chelo.reyes.searchml.databinding.FragmentItemDetailBinding
import io.chelo.reyes.searchml.item.viewmodel.ItemDetailViewModel
import io.chelo.reyes.searchml.item.viewmodel.ItemDetailViewModelFactory

class ItemDetailFragment : Fragment() {

    private val viewModel: ItemDetailViewModel by lazy {
        ViewModelProvider(this, ItemDetailViewModelFactory())
            .get(ItemDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentItemDetailBinding.inflate(inflater, container, false)
        binding.buttonItemDetailBuy.setOnClickListener {
            navigateToAbout()
        }
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val safeArgs: ItemDetailFragmentArgs by navArgs()
        viewModel.getItemDetail(safeArgs.itemId)
        viewModel.onError().observe(viewLifecycleOwner, Observer {
            showErrorSnack()
        })
    }

    private fun navigateToAbout() {
        val action = ItemDetailFragmentDirections.actionItemDetailFragmentToAboutFragment()
        findNavController().navigate(action)
    }

    private fun showErrorSnack() {
        activity?.let { activity ->
            val view: View = activity.findViewById(android.R.id.content)
            Snackbar.make(view, R.string.item_detail_error_message, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.item_detail_action_back) {
                    findNavController().navigateUp()
                }
                .show()
        }
    }
}