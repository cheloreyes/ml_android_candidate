package io.chelo.reyes.searchml.di

import dagger.Component
import io.chelo.reyes.searchml.core.di.CoreModule
import io.chelo.reyes.searchml.item.viewmodel.ItemDetailViewModelFactory
import io.chelo.reyes.searchml.search.viewmodel.SearchViewModelFactory
import javax.inject.Singleton

@Singleton
@Component(modules = [CoreModule::class])
interface AppComponent {
    fun inject(factory: SearchViewModelFactory)
    fun inject(factory: ItemDetailViewModelFactory)
}