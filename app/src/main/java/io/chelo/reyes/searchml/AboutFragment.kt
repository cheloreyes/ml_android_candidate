package io.chelo.reyes.searchml

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.chelo.reyes.searchml.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentAboutBinding.inflate(inflater, container, false)
        setListeners(binding)
        return binding.root
    }

    private fun setListeners(binding: FragmentAboutBinding) {
        with(binding) {
            listOf(textAboutMail, textAboutGithub).forEach { textView ->
                textView.setOnClickListener {
                    when (it) {
                        textAboutMail -> startIntent("mailto:marceloreyescorrea@gmail.com")
                        textAboutGithub -> startIntent("https://github.com/cheloreyes")
                    }
                }
            }
        }
    }

    private fun startIntent(uriString: String) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(uriString)
        )
        startActivity(intent)
    }
}