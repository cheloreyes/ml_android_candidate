package io.chelo.reyes.searchml.item.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.chelo.reyes.searchml.core.repositories.ItemRepository
import io.chelo.reyes.searchml.di.AppComponentManager
import javax.inject.Inject

class ItemDetailViewModelFactory : ViewModelProvider.Factory {
    @Inject
    lateinit var itemRepository: ItemRepository

    init {
        AppComponentManager.appComponent.inject(this)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemDetailViewModel::class.java)) {
            return modelClass.getConstructor(ItemRepository::class.java)
                .newInstance(itemRepository)
        }
        throw IllegalStateException("ViewModel must extend ${modelClass.name}")
    }

}
