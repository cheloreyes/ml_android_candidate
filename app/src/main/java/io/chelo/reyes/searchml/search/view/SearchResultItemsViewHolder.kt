package io.chelo.reyes.searchml.search.view

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import io.chelo.reyes.searchml.R
import io.chelo.reyes.searchml.databinding.ItemEmptyStateLayoutBinding
import io.chelo.reyes.searchml.databinding.ItemLayoutBinding
import io.chelo.reyes.searchml.databinding.ItemShimmerLayoutBinding
import io.chelo.reyes.searchml.search.viewmodel.EmptyItem
import io.chelo.reyes.searchml.search.viewmodel.ResultItemViewModel
import io.chelo.reyes.searchml.search.viewmodel.SearchResultItem

sealed class SearchResultItemsViewHolder(binding: ViewBinding) :
    RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(item: SearchResultItem)

    class Item(private val binding: ItemLayoutBinding) : SearchResultItemsViewHolder(binding) {
        companion object {
            const val viewType = R.layout.item_layout
        }

        override fun bind(viewModel: SearchResultItem) {
            binding.viewModel = viewModel as ResultItemViewModel
        }
    }

    class LoadingState(binding: ItemShimmerLayoutBinding) :
        SearchResultItemsViewHolder(binding) {
        companion object {
            const val viewType = R.layout.item_shimmer_layout
        }

        override fun bind(item: SearchResultItem) {}
    }

    class EmptyState(private val binding: ItemEmptyStateLayoutBinding) :
        SearchResultItemsViewHolder(binding) {
        companion object {
            const val viewType = R.layout.item_empty_state_layout
        }

        override fun bind(item: SearchResultItem) {
            val drawableRes =
                if (item is EmptyItem) R.drawable.ic_search_orange_24 else R.drawable.ic_search_failed_orange_24
            val stringRes =
                if (item is EmptyItem) R.string.search_empty_state_description else R.string.search_failed_description
            binding.drawable = ContextCompat.getDrawable(binding.root.context, drawableRes)
            binding.message = binding.root.context.getString(stringRes)
        }
    }
}

