package io.chelo.reyes.searchml

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("app:load_image")
fun loadImage(imageView: ImageView, imageUrl: String?) {
    Glide.with(imageView.context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .placeholder(R.drawable.ic_search_loading_24)
                .fitCenter()
                .error(R.drawable.ic_search_error_24)
        )
        .into(imageView)
}

@BindingAdapter("app:gone_unless")
fun hideViewUnless(view: View, gone: Boolean) {
    view.visibility = if (gone) View.VISIBLE else View.GONE
}