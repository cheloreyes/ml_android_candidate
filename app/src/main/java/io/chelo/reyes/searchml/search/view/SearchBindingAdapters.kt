package io.chelo.reyes.searchml.search.view


import android.widget.SearchView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import io.chelo.reyes.searchml.search.viewmodel.SearchQueryTextListener
import io.chelo.reyes.searchml.search.viewmodel.SearchResultItem

@BindingAdapter("search:results")
fun setSearchResultItemsAdapter(recyclerView: RecyclerView, items: List<SearchResultItem>) {
    recyclerView.adapter?.let {
        val adapter = it as SearchResultItemsAdapter
        adapter.submitList(items)
        return
    }
    recyclerView.adapter = SearchResultItemsAdapter()
        .apply { submitList(items) }
}

@BindingAdapter("search:listener")
fun setSearchViewListeners(searchView: SearchView, onQueryTextListener: SearchQueryTextListener) {

    searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return query?.let {
                true.also {
                    searchView.clearFocus()
                    onQueryTextListener.onQueryTextSubmit(query)
                }
            } ?: false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return newText?.let { true.also { onQueryTextListener.onQueryTextChange(newText) } }
                ?: false
        }
    })
}