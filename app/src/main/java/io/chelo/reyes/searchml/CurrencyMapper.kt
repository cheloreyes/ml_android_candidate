package io.chelo.reyes.searchml

import java.text.NumberFormat
import java.util.*

object CurrencyMapper {
    fun getFormattedPrice(price: Double, currency: String? = null): String {
        val formatter = NumberFormat.getCurrencyInstance(Locale.getDefault())
        currency?.let {
            formatter.currency = getCurrency(it)
        }
        return getFormattedPrice(price, formatter)
    }

    fun getFormattedPrice(price: Int, currency: String? = null): String =
        getFormattedPrice(price.toDouble(), currency)

    private fun getFormattedPrice(price: Double, numberFormat: NumberFormat): String =
        numberFormat.format(price)

    private fun getCurrency(currency: String): Currency = try {
        Currency.getInstance(currency)
    } catch (ex: IllegalArgumentException) {
        Currency.getInstance(Locale.getDefault())
    }
}