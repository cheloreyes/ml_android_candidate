package io.chelo.reyes.searchml.search.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.chelo.reyes.searchml.core.repositories.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.ReplaySubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

private const val DEBOUNCE_TIMEOUT_MILLIS: Long = 500
private const val MAX_LOADING_ITEMS: Int = 10

class SearchViewModel(private val searchRepository: SearchRepository) : ViewModel(),
    SearchQueryTextListener {

    val items = ObservableField<List<SearchResultItem>>()

    private val disposeBag = CompositeDisposable()
    private val searchSubject = ReplaySubject.createWithSize<String>(1)

    private val onItemSelected = MutableLiveData<String>()

    init {
        setEmptyState()
        disposeBag.add(searchSubject
            .debounce(DEBOUNCE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS, Schedulers.io())
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                search(it)
            })
    }

    override fun onCleared() {
        disposeBag.clear()
        super.onCleared()
    }

    override fun onQueryTextSubmit(query: String) {
        search(query)
    }

    override fun onQueryTextChange(text: String) {
        searchSubject.onNext(text)
    }

    fun onItemSelected(): LiveData<String> = onItemSelected

    private fun search(query: String) {
        setLoadingState()
        disposeBag.add(
            searchRepository.search(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    result.results.let {
                        if (it.isEmpty()) {
                            setEmptyState()
                            return@subscribe
                        }
                        items.set(it.map { itemModel ->
                            ResultItemViewModel(itemModel) { itemSelected ->
                                onItemSelected.value = itemSelected.id
                                resetSelection()
                            }
                        })
                    }
                }, {
                    setNoResultsState()
                    Timber.e(it)
                })
        )

    }

    private fun setEmptyState() = items.set(listOf(EmptyItem()))

    private fun setNoResultsState() = items.set(listOf(NoResultsItem()))

    private fun setLoadingState() = items.set((0..MAX_LOADING_ITEMS).toList().map { LoadingItem() })

    private fun resetSelection() {
        onItemSelected.value = ""
    }
}

interface SearchQueryTextListener {
    fun onQueryTextSubmit(query: String)
    fun onQueryTextChange(text: String)
}