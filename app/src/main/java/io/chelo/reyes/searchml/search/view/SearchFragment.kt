package io.chelo.reyes.searchml.search.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import io.chelo.reyes.searchml.databinding.FragmentSearchBinding
import io.chelo.reyes.searchml.search.viewmodel.SearchViewModel
import io.chelo.reyes.searchml.search.viewmodel.SearchViewModelFactory

class SearchFragment : Fragment() {

    private val viewModel: SearchViewModel by lazy {
        ViewModelProvider(this, SearchViewModelFactory())
            .get(SearchViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSearchBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onItemSelected().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val action = SearchFragmentDirections.actionSearchFragmentToItemDetailFragment(it)
                findNavController().navigate(action)
            }
        })
    }

}