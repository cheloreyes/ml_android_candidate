package io.chelo.reyes.searchml.di

object AppComponentManager {
    lateinit var appComponent: AppComponent
        private set

    fun init(component: AppComponent) {
        appComponent = component
    }
}