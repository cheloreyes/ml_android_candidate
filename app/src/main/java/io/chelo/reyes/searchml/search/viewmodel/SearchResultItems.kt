package io.chelo.reyes.searchml.search.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import io.chelo.reyes.searchml.CurrencyMapper
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface

interface SearchResultItem

class EmptyItem : SearchResultItem

class NoResultsItem : SearchResultItem

class LoadingItem : SearchResultItem

class ResultItemViewModel(
    val id: String,
    private val onItemClicked: (ResultItemViewModel) -> Unit
) :
    ViewModel(), SearchResultItem {

    val itemImageUrl = ObservableField<String>()
    val itemName = ObservableField<String>()
    val itemPrice = ObservableField<String>()
    val itemInstallmentPrice = ObservableField<String>()
    val itemInstallmentMultiplier = ObservableInt()
    val itemIsFreeShipping = ObservableBoolean(false)
    val itemInstallmentsAvailable = ObservableBoolean(false)

    constructor(item: ItemModelInterface, onItemClicked: (ResultItemViewModel) -> Unit) : this(
        item.id,
        onItemClicked
    ) {
        itemImageUrl.set(item.thumbnail)
        itemName.set(item.title)
        itemPrice.set(CurrencyMapper.getFormattedPrice(item.price, item.currencyId))
        item.installments?.let {
            itemInstallmentMultiplier.set(it.quantity)
            itemInstallmentPrice.set(
                CurrencyMapper.getFormattedPrice(it.amount, it.currencyId)
            )
            itemInstallmentsAvailable.set(true)
        }
        itemIsFreeShipping.set(item.shipping.freeShipping)
    }

    fun onItemClick() {
        onItemClicked.invoke(this)
    }
}