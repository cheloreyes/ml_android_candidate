package io.chelo.reyes.searchml

import android.app.Application
import io.chelo.reyes.searchml.core.di.CoreModule
import io.chelo.reyes.searchml.di.AppComponentManager
import io.chelo.reyes.searchml.di.DaggerAppComponent

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val appComponent = DaggerAppComponent.builder()
            .coreModule(CoreModule())
            .build()
        AppComponentManager.init(appComponent)
    }
}