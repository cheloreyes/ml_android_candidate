package io.chelo.reyes.searchml.item.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.chelo.reyes.searchml.CurrencyMapper
import io.chelo.reyes.searchml.core.models.item.ConditionType
import io.chelo.reyes.searchml.core.models.item.ItemModelInterface
import io.chelo.reyes.searchml.core.repositories.ItemRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber

private const val EXPECTED_IMAGE_MAX_SIZE: String = "1276x932"

class ItemDetailViewModel(private val itemRepository: ItemRepository) : ViewModel() {
    val itemHighResImage = ObservableField<String>()
    val itemCondition = ObservableField<ConditionType>()
    val itemQuantitySold = ObservableInt()
    val itemTitle = ObservableField<String>()
    val itemCurrentPrice = ObservableField<String>()
    val itemStock = ObservableInt()
    val isLoadingData = ObservableBoolean(true)

    private val disposeBag = CompositeDisposable()

    private val onError = MutableLiveData<Unit>()

    init {
        listOf(
            itemHighResImage,
            itemTitle,
            itemCurrentPrice
        ).forEach { it.set("") }
    }

    override fun onCleared() {
        disposeBag.clear()
        super.onCleared()
    }

    fun getItemDetail(itemId: String) {
        disposeBag.add(
            itemRepository.getItemDetails(itemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setDetails(it)
                }, {
                    Timber.e(it)
                    onError.postValue(Unit)
                })
        )
    }

    fun onError(): LiveData<Unit> = this.onError

    fun onBuyAction() {

    }

    private fun setDetails(itemDetail: ItemModelInterface) {
        with(itemDetail) {
            val picture =
                pictures.find { it.maxSize == EXPECTED_IMAGE_MAX_SIZE } ?: pictures.first()
            itemHighResImage.set(picture.secureUrl)
            itemCondition.set(condition)
            itemQuantitySold.set(soldQuantity)
            itemTitle.set(title)
            itemCurrentPrice.set(CurrencyMapper.getFormattedPrice(price, currencyId))
            itemStock.set(availableQuantity)
            isLoadingData.set(false)
        }
    }
}