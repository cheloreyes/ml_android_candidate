Hola! soy Marcelo Reyes Correa, Ingeniero de sistemas y desarrollador de apps Android especialmente. gracias por atender mi solicitud para trabajar con ustedes, esta es mi propuesta para el ejercicio de ***Mercadolibre Android candidate***

Para el desarrollo de este ejercicio quise utilizar el lenguaje **Kotlin** con Android Studio 4.0 y arquitectura **MVVM**. 
Para esto lo implementé en dos módulos:

 - **App**: Contiene la implementación para la Vista y sus ViewModel asociados, además de layouts y recursos necesarios. 
 - **Core**: Contiene la implementación para el acceso y tratamiento de datos, usando el patrón de repositorios y exponiendo un acceso genérico de interfaces utilizando RxJava para una comunicación asíncrona.

Entre las librerías utilizadas están **Dagger** para la inyección de dependencias, **Navigation** para gestionar la navegación de la aplicación, **DataBinding** para actualizar los elementos de vista de manera reactiva, **ViewModel** para la declaración de la información relacionada a las vistas, todo esto teniendo en cuenta los lineamientos de Android Jetpack y sus guías de desarrollo

En este repositorio pueden conocer la implementación propuesta o probar la aplicación demo **[APK](apk/app-debug.apk)**

| Empty view | Results view | Detail view |
| ------ | ------ | ------ |
|![](screenshots/Screenshot_1593985713.png) | ![](screenshots/Screenshot_1593985777.png) | ![](screenshots/Screenshot_1593985784.png)



